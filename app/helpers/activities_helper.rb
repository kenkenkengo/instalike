module ActivitiesHelper
  def unchecked_activities
    @activities = current_user.activities.where(read: false)
  end
end
