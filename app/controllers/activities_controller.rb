class ActivitiesController < ApplicationController
  before_action :user_signed_in?, only: %i(read)
  def read
    activity = current_user.activities.find(params[:id])
    activity.read! if activity.unread?
    redirect_to post_path(activity.subject.post)
  end
end
