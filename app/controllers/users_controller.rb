class UsersController < ApplicationController
  before_action :user_signed_in?, only: [:show, :index, :following, :followers]

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.includes(:photos).paginate(page: params[:page], per_page: 3).order('created_at DESC')
  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def following
    @title = "フォロー一覧"
    @user = User.find_by(id: params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "フォロワー一覧"
    @user = User.find_by(id: params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
end
