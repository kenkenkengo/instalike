class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: %i(show destroy)

  def new
    @post = Post.new
    @post.photos.build
  end

  def create
    @post = Post.new(post_params)
    if @post.photos.present?
      @post.save
      redirect_to posts_path
      flash[:notice] = "投稿が保存されました"
    else
      redirect_to new_post_path
      flash[:alert] = "投稿に失敗しました"
    end
  end

  def index
    @posts = current_user.feed.limit(5).includes(:photos, :user).paginate(page: params[:page], per_page: 4).order('created_at DESC')
  end

  def show
  end

  def destroy
    if @post.user == current_user
      @post.destroy if @post.destroy
      flash[:notice] = "投稿が削除されました"
    else
      flash[:alert] = "投稿削除に失敗しました"
    end
    redirect_to root_path
  end

  def search
    @search_results = Post.search(params[:searchkey])
    render 'posts/search'
  end

  private

  def post_params
    params.require(:post).permit(:caption, photos_attributes: [:image]).merge(user_id: current_user.id)
  end

  def set_post
    @post = Post.find_by(id: params[:id])
  end
end
