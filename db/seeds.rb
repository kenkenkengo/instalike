# メインのサンプルユーザーを1人作成する
User.create!(name:  "Example User",
             username: "apple",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar")

# 追加のユーザーをまとめて生成する
99.times do |n|
  name  = Faker::Name.name
  username = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               username: username,
               email: email,
               password:              password,
               password_confirmation: password)
end

#一部のユーザーにpostを生成
users = User.order(:created_at).take(5)
4.times do
  users.each do |user|
    caption = Faker::Lorem.sentence(word_count: 5)
    post = user.posts.create!(caption: caption)
    post.photos.create!(image: File.open('/Users/yamamotokengo/environment/instalike/app/assets/images/D8CE060F-5C39-48A9-92CA-0578D3ED2928.JPEG'))
  end
end


# リレーションシップ作成
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each {|followed|user.follow(followed)}
followers.each {|follower|follower.follow(user)}